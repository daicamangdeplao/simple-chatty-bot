import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        // put your code here
        Scanner scanner = new Scanner(System.in);

//        var a = scanner.nextInt();
//        var b = scanner.nextInt();
        var input = scanner.nextLine().split(" ");

//        System.out.println("Input: " + input[0]);
//        System.out.println("Input: " + input[1]);

        var result = 1l;

        for (int i = Integer.parseInt(input[0]); i < Integer.parseInt(input[1]); i++) {
//            System.out.println("element: " + i);
            result *= i;
        }

        System.out.println(result);
    }
}

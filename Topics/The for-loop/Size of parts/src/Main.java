import java.util.Scanner;

class Main {

    private static int ACCEPT = 0;
    private static int REJECT = -1;
    private static int FIXED = 1;

    public static void main(String[] args) {
        // put your code here
        Scanner scanner = new Scanner(System.in);

        var n = scanner.nextInt();
        var temp = 0;
        var countFixed = 0;
        var countReject = 0;
        var countAccept = 0;

        for (int i = 0; i < n; i++) {
            temp = scanner.nextInt();
            if (temp == ACCEPT) {
                countAccept++;
            }

            if (temp == REJECT) {
                countReject++;
            }

            if (temp == FIXED) {
                countFixed++;
            }
        }

        System.out.print(countAccept);
        System.out.print(" ");
        System.out.print(countFixed);
        System.out.print(" ");
        System.out.print(countReject);
    }
}

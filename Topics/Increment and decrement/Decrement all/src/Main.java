import java.util.Arrays;
import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        var input = scanner.nextLine();
        final String[] numbers = input.split(" ");
        Arrays.stream(numbers)
                .map(Integer::parseInt)
                .map(n -> --n)
                .forEach(n -> System.out.print(n + " "));
    }
}

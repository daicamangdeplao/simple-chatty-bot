import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        // put your code here
        Scanner scanner = new Scanner(System.in);
        var input = scanner.nextLine();
        System.out.println(isAtTheEnd(input));
    }

    public static boolean isAtTheEnd(String input) {
        final String[] tokens = input.split(" ");
        final int numberTokens = tokens.length;

        return tokens[numberTokens - 1].contains("burg");
    }
}

import java.util.Arrays;
import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        // put your code here
        Scanner scanner = new Scanner(System.in);

        String str1 = scanner.nextLine().trim();
        String str2 = scanner.nextLine().trim();

        str1 = getRidOfSpace(str1);
        str2 = getRidOfSpace(str2);

        System.out.println(str1.equals(str2));
    }

    private static String getRidOfSpace(String str) {
        String result = "";
        String[] tokens = str.split(" ");
        if (tokens.length > 1) {
            for (String t : tokens) {
                result = result.concat(t);
            }
            return result;
        }
        return str;
    }
}

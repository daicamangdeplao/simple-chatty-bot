import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        // put your code here
        Scanner scanner = new Scanner(System.in);

        final int a = scanner.nextInt();
        final int b = scanner.nextInt();
        final int c = scanner.nextInt();

        if (isTriangle(a, b, c)) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }

    private static boolean isTriangle(int a, int b, int c) {
        return ((a + b) > c) && ((a + c) > b) && ((c + b) > a);
    }
}

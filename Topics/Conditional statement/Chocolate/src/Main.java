import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        // put your code here
        Scanner scanner = new Scanner(System.in);

        final int N = scanner.nextInt();
        final int M = scanner.nextInt();
        final int K = scanner.nextInt();

        int totalSegments = N * M;

        boolean breakable = breakChocolateBar(N, K, totalSegments);
        if (breakable) {
            System.out.println("YES");
        } else {
            breakable = breakChocolateBar(M, K, totalSegments);
            if (breakable) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
        }
    }

    private static boolean breakChocolateBar(int n, int k, int sumSegments) {
        return isBreakable(n, k, sumSegments);
    }

    private static boolean isBreakable(int numberOfSegmentPerBreak, int k, int sumSegments) {
        int i = 1;
        int remainder = 0;
        while (true) {
            remainder = sumSegments - numberOfSegmentPerBreak * i++;
            if (remainder <= 0 || remainder < k) break;
            if (remainder == k) {
                return true;
            }
        }
        return false;
    }
}

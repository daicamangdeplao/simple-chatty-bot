//put imports you need here

import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        // put your code here
        Scanner scanner = new Scanner(System.in);

        final String firstName = scanner.next();
        final String yearsOfExperience = scanner.next();
        final String cuisinePreference = scanner.next();

        System.out.println(String.format("The form for %s is completed. We will contact you if we need a chef who cooks %s dishes and has %s years of experience.",
                firstName, cuisinePreference, yearsOfExperience));

    }
}

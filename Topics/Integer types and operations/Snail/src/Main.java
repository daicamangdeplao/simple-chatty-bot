import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // put your code here

        final int height = scanner.nextInt();
        final int workloadMorning = scanner.nextInt();
        final int workloadNight = scanner.nextInt();
        int road = 0;
        int day = 1;

        while (true) {
            road = road + workloadMorning;
            if (road >= height) {
                break;
            }
            day = day + 1;
            road = road - workloadNight;
        }

        System.out.println(day);

    }
}
